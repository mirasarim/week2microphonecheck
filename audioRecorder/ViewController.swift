//
//  ViewController.swift
//  audioRecorder
//
//  Created by Temirlan Merekeyev on 26.07.17.
//  Copyright © 2017 Temirlan Merekeyev. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var micBtn: UIButton!
    
    @IBOutlet weak var recoredView: UIView!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    var audioFileName : String = ""
    var arrayOfFileNames : [String] = []
    var array : [String] = []
    var wasRecord = false
    @IBOutlet weak var audioFilesTableView: UITableView!
    
    @IBOutlet weak var playBtn: UIButton!
    
    @IBAction func playFile(_ sender: Any) {
        
        
        
        if AudioPlayerManager.shared.isPlaying{
            
            AudioPlayerManager.shared.pause()
            self.playBtn.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            
        }else{
            
            let path = AudioPlayerManager.shared.audioFileInUserDocuments(fileName: audioFileName)
            self.playBtn.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            
            AudioPlayerManager.shared.play(path: path)
            
        }
        
    }
    
    
    
    
    //MARK:
    //MARK: VIEW
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.arrayOfFileNames = contentsOfDirectoryAtPath(path: searchPath)!
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.audioPlayerDidFinishPlaying), name: NSNotification.Name.AudioPlayerDidFinishPlayingAudioFile, object: nil)
        
        
        //
    }
    
    func contentsOfDirectoryAtPath(path: String) -> [String]? {
        guard let paths = try? FileManager.default.contentsOfDirectory(atPath: path) else { return nil}
        return paths.map { aContent in (path as NSString).appendingPathComponent(aContent)}
    }
    
    let searchPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
    
    
    
    func getUserDocumentsPath()->URL{
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        return documentsURL
    }
    
    func audioPlayerDidFinishPlaying(){
        
        print("Audio player did finish")
        
        DispatchQueue.main.async {
            self.playBtn.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Timer to update the status of our recording
    var nonObservablePropertiesUpdateTimer = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.main)
    
    
    //MARK:
    //MARK: Recored Button
    
    @IBAction func StartRecording(_ sender: UIButton) {
        wasRecord = true
        audioFileName = NSUUID().uuidString + ".m4a"
        
        self.arrayOfFileNames.append(audioFileName)
        
        AudioRecorderManager.shared.recored(fileName: audioFileName) { (status:Bool) in
            
            if status == true{
                print("Did Start recording")
            }else{
                
                print("Erro starting recorder")
            }
            
        }
        
        
        DispatchQueue.main.async {
            self.statusLabel.text = "Release to stop recording"
            self.statusLabel.textColor = UIColor.orange
        }
        
    }
    
    @IBAction func stopRecording(_ sender: UIButton) {
        
        
        AudioRecorderManager.shared.finishRecording()
        self.audioFilesTableView.reloadData()
        
        UIView.animate(withDuration: 0.3, animations: {
            //            self.WaveAnimationView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        })
        nonObservablePropertiesUpdateTimer.suspend()
        
        DispatchQueue.main.async {
            self.statusLabel.text = "Press & hold to start"
            self.statusLabel.textColor = UIColor.green
        }
    }
    
}


extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfFileNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let audioFile = arrayOfFileNames[indexPath.row]
        
        
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            ?? UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        
        let fileName = audioFile.components(separatedBy: searchPath + "/")
        let audio = fileName.last!
        
        cell.textLabel?.text = audio
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var audioFile = arrayOfFileNames[indexPath.row]
        if audioFile.range(of: ".m4a.m4a")  != nil{
            let endIndex = audioFile.index(audioFile.endIndex, offsetBy: -4)
            let truncated = audioFile.substring(to: endIndex)
            let newAudiFileName = truncated.components(separatedBy: searchPath + "/")
            audioFile = newAudiFileName.last!
            
        }
       
        if AudioPlayerManager.shared.isPlaying{
            
            AudioPlayerManager.shared.pause()
            self.playBtn.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            
        }else{
            
                let path = AudioPlayerManager.shared.audioFileInUserDocuments(fileName: audioFile)
                self.playBtn.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                AudioPlayerManager.shared.play(path: path)
            }
        }
        
    }
    
    
    
