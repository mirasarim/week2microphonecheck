//
//  AudioRecorderManager.swift
//  audioRecorder
//
//  Created by Temirlan Merekeyev on 26.07.17.
//  Copyright © 2017 Temirlan Merekeyev. All rights reserved.
//


import UIKit
import AVFoundation





class AudioRecorderManager: NSObject {
    
    
    
    static let shared = AudioRecorderManager()
    
    var recordingSession: AVAudioSession!
    var recorder:AVAudioRecorder?
    
    func setup(){
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with: [.defaultToSpeaker])
            try recordingSession.setActive(true)
            
            recordingSession.requestRecordPermission() {[weak self] (allowed: Bool) -> Void  in
                
                if allowed {
                    print("Recording Allowed")
                    
                } else {
                    print("Recording Allowed Failed")
                    
                }
                
            }
            
            
        } catch {
            print("Faild to setCategory",error.localizedDescription)
        }
        
        guard self.recordingSession != nil else {
            print("Error session is nil")
            return
        }
        
        
        
    }
    
    
    
    
    
    func getUserDocumentsPath()->URL{
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        return documentsURL
    }
    
    
    func recored(fileName:String,result:(_ isRecording:Bool)->Void) {
        
        
        
        let path = getUserDocumentsPath().appendingPathComponent(fileName+".m4a")
        
        let audioURL = NSURL(fileURLWithPath: path.path)
        
        
        let recoredSt:[String:Any] = [
            AVFormatIDKey:NSNumber(value: kAudioFormatAppleLossless),
            AVEncoderAudioQualityKey : AVAudioQuality.high.rawValue,
            AVEncoderBitRateKey : 12000.0,
            AVNumberOfChannelsKey: 1,
            AVSampleRateKey : 44100.0
            
        ]
        
        
        
        do {
            recorder = try AVAudioRecorder(url: audioURL as URL, settings: recoredSt)
            recorder?.delegate = self
            
            recorder?.isMeteringEnabled = true
            recorder?.prepareToRecord()
            
            recorder?.record()
            
            
            result(true)
            print("Recording")
        } catch {
            result(false)
        }
    }
    
    
    var recorderTime: String?
    var recorderApc0: Float?
    var recorderPeak0:Float?
    
   
    
    
    func finishRecording(){
        
        self.recorder?.stop()
        
    }
    
    
    
    
}

extension AudioRecorderManager:AVAudioRecorderDelegate{
    
    // Audio Recorder Delegate
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
        print("Audio Recorder did finish",flag)
    }
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder,
                                          error: Error?) {
        print("\(String(describing: error?.localizedDescription))")
    }
    
}

